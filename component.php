<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

 
CModule::IncludeModule('iblock');
if ($this->StartResultCache($arParams['CACHE_TIME']))
{
    $arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "ID"=>$arParams['ID']);
	$res = CIBlockElement::GetList(Array(), $arFilter);
	if ($ob = $res->GetNextElement()){;
		$arProps = $ob->GetProperties();
	}

	$arFilterTypes = Array("IBLOCK_ID"=>1, "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilterTypes, false, false, array('ID','NAME'));
	$sportTypes = array();
	while($ob = $res->GetNext())
	{
		$sportTypes[$ob['ID']] = $ob['NAME'];
	}

	
	$arSort= Array("DATE_ACTIVE_FROM"=>"DESC");
	$arSelect = Array();
	$arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], "!ID"=>$arParams['ID'], "PROPERTY_SPORT_TYPES" => $arProps['SPORT_TYPES']['VALUE']);

	$res =  CIBlockElement :: GetList ($arSort, $arFilter, false, array('nTopCount' => $arParams['ELEMENT_COUNT']), $arSelect);

	while($ar_result = $res->GetNextElement())
	{
		$arProps = $ar_result->GetProperties();
		$arFields = $ar_result->GetFields();
		$arResult[$arFields['ID']] = $arFields;
		
		foreach($arProps['SPORT_TYPES']['VALUE'] as $prop)
		{
			$arResult[$arFields['ID']]['CATS'][$prop] = $sportTypes[$prop];
		}
		
		$arResult[$arFields['ID']]["DATE"] = CIBlockFormatProperties::DateFormat($arParams["DATE_FORMAT"], MakeTimeStamp($arFields["ACTIVE_FROM"], CSite::GetDateFormat()));

		$arResult[$arFields['ID']]['CATS_LIST'] = join(", ", $arResult[$arFields['ID']]['CATS']);
	}
	

    $this->IncludeComponentTemplate();
}
?>