<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?if(sizeof($arResult) > 0):?>
	<div class="news-list">

		<?foreach($arResult as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<p class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<span><?echo $arItem["DATE"]?></span>
				<?=$arItem["NAME"];?>
				<?if($arItem["CATS_LIST"] != ""):?>
					(<?=$arItem["CATS_LIST"];?>)
				<?endif;?>
			</p>
		<?endforeach;?>
	</div>
<?endif;?>