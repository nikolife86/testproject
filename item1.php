<?
final class Init {
	
	private $dbHost = "";
	private $dbUser = "";
	private $dbPass = "";
	private $dbName = "";
	
	private $dbTableName = "test";
	
	private $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ2345678';
	
	private $fieldScriptName = "";
	private $fieldStartTime = "";
	private $fieldEndTime = "";
	private $fieldResult = "";
	private $fieldResultArr = array(
		'1' => 'normal',
		'2' => 'illegal',
		'3' => 'failed',
		'4' => 'success'
	);

	
	public function __construct()
	{		
		$chars = $this->chars;
		$numChars = strlen($chars);
		$string = '';
		for ($i = 0; $i < rand(); $i++) {
			$string .= substr($chars, rand(1, $numChars) - 1, 1);
		}
		
		$this->fieldScriptName = $string;
		$this->fieldStartTime = rand();
		$this->fieldEndTime = rand();
		
		
		$arr = $this->fieldResultArr;
		$arrId = rand(1, 4);
		$this->fieldResult = $arr[$arrId];
		
		$this->create();
		$this->fill();
	}
	
    private function create()
	{
		mysql_connect($this->dbHost, $this->dbUser, $this->dbPass) or die(mysql_error());
		mysql_select_db($this->dbName) or die(mysql_error());

		mysql_query("CREATE TABLE IF NOT EXISTS ".$this->dbTableName." 
				(
					id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
					script_name VARCHAR(25),
					start_time INT,
					end_time INT,
					result ENUM('normal', 'illegal', 'failed', 'success') NOT NULL 
				) DEFAULT CHARSET=utf8") or die(mysql_error());
		mysql_close ();
		
		return true;
	}
	
	private function fill()
	{
		mysql_connect($this->dbHost, $this->dbUser, $this->dbPass) or die(mysql_error());
		mysql_select_db($this->dbName) or die(mysql_error());

		mysql_query("INSERT INTO ".$this->dbTableName." (script_name, start_time, end_time, result) 
			VALUES ('".$this->fieldScriptName."', '".$this->fieldStartTime."', '".$this->fieldEndTime."', '".$this->fieldResult."')") or die(mysql_error());
		mysql_close ();
		
		return true;
	}
	
	public function get()
	{
		mysql_connect($this->dbHost, $this->dbUser, $this->dbPass) or die(mysql_error());
		mysql_select_db($this->dbName) or die(mysql_error());

		$result = mysql_query("SELECT * FROM ".$this->dbTableName." WHERE result IN ('normal','success')") or die(mysql_error());
		while ($row = mysql_fetch_assoc($result)) {
			$myArr[] = $row;
		}
		
		mysql_close ();
		
		if(!sizeof($myArr)) {
			throw new Exception('В базе нет данных по вашему запросу');
		}
		
		return $myArr;
	}

}



try
{
    $My = new Init();
	print_r($My->get());
} catch (Exception $e) {
	echo 'Error :' . $e->getMessage() . '<br />';
	exit();

}



?>