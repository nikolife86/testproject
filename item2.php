<?php

class Files {
	public function getFilesName($dirName)
	{
		$entries = scandir($dirName);
		$filelist = array();
		foreach($entries as $entry) {
			if( preg_match('~^[[:alnum:]]+\.txt$~', $entry))
			{
				$filelist[] = $entry;
			}
		}
		
		return $filelist;
	}
}


?>